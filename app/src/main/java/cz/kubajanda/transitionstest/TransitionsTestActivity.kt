package cz.kubajanda.transitionstest

import android.animation.Animator
import android.animation.ObjectAnimator
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.core.animation.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.transition.*

class TransitionsTestActivity : AppCompatActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_transitionstest)

    val button = findViewById<Button>(R.id.testButton)
    val buttonContainer = findViewById<ViewGroup>(R.id.buttonContainer)

    button.setOnClickListener {
      showFragment()



      TransitionManager.beginDelayedTransition(
        buttonContainer,
        AutoTransition().setDuration(150).addListener(object : Transition.TransitionListener {
          override fun onTransitionStart(transition: Transition) = Unit

          override fun onTransitionEnd(transition: Transition) {
            val nextTransition = AutoTransition()
            TransitionManager.beginDelayedTransition(
              buttonContainer,
              nextTransition.setDuration(150)
            )
            button.text = "Short text"
          }

          override fun onTransitionCancel(transition: Transition) = Unit

          override fun onTransitionPause(transition: Transition) = Unit

          override fun onTransitionResume(transition: Transition) = Unit
        })
      )
      button.text = "Very very very very long text!"
    }
  }

  private fun showFragment() {
    supportFragmentManager.beginTransaction()
      .replace(R.id.fragmentLayout, CustomTransitionFragment())
      .addToBackStack("testFragment")
      .commit()
  }

  class CustomTransitionFragment : Fragment(R.layout.fragment_transitionstest) {
    override fun onCreate(savedInstanceState: Bundle?) {
      super.onCreate(savedInstanceState)

      enterTransition = createTransition()

      lifecycle.addObserver(object : LifecycleObserver {
        @OnLifecycleEvent(Lifecycle.Event.ON_ANY)
        fun onEvent() {
          Log.d("TransitionTest", "Fragment state: ${lifecycle.currentState}")
        }
      })
    }

    private fun createTransition(): Transition {
      return object : Visibility() {
        override fun onAppear(
          sceneRoot: ViewGroup,
          view: View,
          startValues: TransitionValues?,
          endValues: TransitionValues?
        ): Animator? {
          return ObjectAnimator.ofFloat(view, "alpha", 0f, 1f).apply {
            duration = 500
            doOnStart { Log.d("TransitionTest", "Enter transition start") }
            doOnPause { Log.d("TransitionTest", "Enter transition pause") }
            doOnEnd { Log.d("TransitionTest", "Enter transition end") }
            doOnCancel { Log.d("TransitionTest", "Enter transition cancel") }
            doOnResume { Log.d("TransitionTest", "Enter transition resume") }
          }
        }

        override fun onDisappear(
          sceneRoot: ViewGroup,
          view: View,
          startValues: TransitionValues?,
          endValues: TransitionValues?
        ): Animator? {
          return null
        }
      }
    }
  }
}